from app.sananmuunnos import app
from flask import json
import pytest
"""
What should the sananmuunnos endpoint do?

Make an endpoint that accepts POST requests.

Both the body of the POST request and the response you make are JSON strings (that is, quoted strings that pertain to JSON formatting).

The response string should be formed from the request string with the following rules:

1. For each pair of words, swap the beginnings of the words, up to and including the first vowel (and any consecutive vowels) of the word.
Example: "fooma barbu" becomes "bama foorbu".

2. If there is an odd word at the end of the string, leave that as is.
Example: "hello" becomes "hello".
Example: "foo bar baz" becomes "ba foor baz".

3. Words are separated by spaces; please preserve exact spacing.
Example: "amama   bomomo foo" becomes "bomama   amomo foo"

4. You can treat punctuation as part of words.
Example: "I'd rather die here." becomes "ra'd Ither he diere."

5. Vowels include (at least) those in the Finnish alphabet (a, e, i, o, u, y, å, ä, ö).
Example: "vuoirkage mäölnö" becomes "mäörkage vuoilnö".

6. You can treat as a consonant any character that is not space or a vowel.

Examples:

request body    expected response body
"Donald Trump"  "Trunald Domp"
"Supercalifragilisticexpialidocious!"   "Supercalifragilisticexpialidocious!"
"death, famine, and pestilence" "fath, deamine, pend astilence"
"""

testdata = [
    ("fooma barbu", "bama foorbu"),
    ("hello", "hello"),
    ("foo bar baz", "ba foor baz"),
    ("amama   bomomo foo", "bomama   amomo foo"),
    ("I'd rather die here.", "ra'd Ither he diere."),
    ("vuoirkage mäölnö", "mäörkage vuoilnö"),
    ("Donald Trump", "Trunald Domp"),
    ("Supercalifragilisticexpialidocious!", "Supercalifragilisticexpialidocious!"),
    ("death, famine, and pestilence", "fath, deamine, pend astilence"),
    ("šaåðäcxs jiy kah puĸåäkäcift", "jiyðäcxs šaå puh kaĸåäkäcift"),
]

@pytest.mark.parametrize("given,result",testdata)
def test_sananmuunnos(given:str, result:str):
    response=app.test_client().post('/', 
                                data=json.dumps(given), 
                                content_type='application/json')
    assert response.status_code == 200
    assert response.get_json() == result