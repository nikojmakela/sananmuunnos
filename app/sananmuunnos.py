#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import re, typing
from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/', methods=['POST'])
def sanamuunnos():
    """
    What should the sananmuunnos endpoint do?

    Make an endpoint that accepts POST requests.

    Both the body of the POST request and the response you make are JSON strings (that is, quoted strings that pertain to JSON formatting).

    The response string should be formed from the request string with the following rules:

    1. For each pair of words, swap the beginnings of the words, up to and including the first vowel (and any consecutive vowels) of the word.
    Example: "fooma barbu" becomes "bama foorbu".

    2. If there is an odd word at the end of the string, leave that as is.
    Example: "hello" becomes "hello".
    Example: "foo bar baz" becomes "ba foor baz".

    3. Words are separated by spaces; please preserve exact spacing.
    Example: "amama   bomomo foo" becomes "bomama   amomo foo"

    4. You can treat punctuation as part of words.
    Example: "I'd rather die here." becomes "ra'd Ither he diere."

    5. Vowels include (at least) those in the Finnish alphabet (a, e, i, o, u, y, å, ä, ö).
    Example: "vuoirkage mäölnö" becomes "mäörkage vuoilnö".

    6. You can treat as a consonant any character that is not space or a vowel.

    Examples:

    request body    expected response body
    "Donald Trump"  "Trunald Domp"
    "Supercalifragilisticexpialidocious!"   "Supercalifragilisticexpialidocious!"
    "death, famine, and pestilence" "fath, deamine, pend astilence"
    """
    content: str = request.get_json()
    splitted: list[str] = black_white_split(content)
    result: str = ""
    pairs: list[str] = []
    separator: str = ''
    index = 0

    # Loop through words
    while (index < len(splitted)):
        word = splitted[index]

        # If pair is cleared but there is a separator set, append it to the result
        if len(separator) and not len(pairs):
            result += separator
            separator = ''

        # Collect pairs and separators
        if len(pairs) <= 2:

            # Add to pairs if a real word
            if re.fullmatch(r'(\S+)', word):
                pairs.append(word)
            # Set as separator if empty
            else:
                separator = word

        # When a pair is ready, append it to a result text and clear pairs.
        if len(pairs) == 2:
            result += transform_words(pairs[0], pairs[1], separator)
            pairs = []
            separator = ''

        # If on last round, use what we got.
        if (index == len(splitted) - 1) and (len(pairs) == 1):
            result += separator + pairs[0]

        # Always raise by one word (until limit is reached)
        index += 1

    # Return a JSON string
    return jsonify(result)

def black_white_split(text:str) -> typing.List[str]:
    """
    1. "fooma barbu" -> ['fooma', ' ', 'barbu']
    2. "amama   bomomo foo" -> ['amama', '   ', 'bomomo', ' ', 'foo']
    """
    return re.split(r'(\s+)', text)


def transform_words(word1:str, word2:str, separator:str = ' ') -> str:
    """
    Split 2 words from first vowel and exchange them between each other.
    
    VOWELS = 'aeiouyåäöAEIOUYÅÄÖ'
    """
    syllables = r'[^aeiouyåäöAEIOUYÅÄÖ\s]*[aeiouyåäöAEIOUYÅÄÖ]+'
    s1: str = re.search(syllables, word1).group(0)
    s2: str = re.search(syllables, word2).group(0)
    result: str = separator.join([word1.replace(s1, s2, 1), word2.replace(s2, s1, 1)])
    return result