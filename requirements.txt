attrs==20.2.0
click==7.1.2
colorama==0.4.4
docopt==0.6.2
Flask==1.1.2
gunicorn==20.0.4
iniconfig==1.1.1
itsdangerous==1.1.0
Jinja2==2.11.2
MarkupSafe==1.1.1
packaging==20.4
pathtools==0.1.2
pluggy==0.13.1
py==1.9.0
pyparsing==2.4.7
pytest==6.1.2
pytest-watch==4.2.0
six==1.15.0
toml==0.10.2
watchdog==0.10.3
Werkzeug==1.0.1
